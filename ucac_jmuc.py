#!/usr/bin/env python

import sys
from astropy.io import fits as pyfits


def ucac_jmuc(ucac,jmuc):

    # converts UCAC4 r,i in Gunn to R,I of Johnson's

    fits = pyfits.open(ucac)
    rmag = fits[1].data['rmag']
    imag = fits[1].data['imag']
    e_rmag = fits[1].data['e_rmag']
    e_imag = fits[1].data['e_imag']

    # http://www.sdss.org/dr4/algorithms/sdssUBVRITransform.html
    # Lupton (2005)
    rjmag = rmag - 0.2936*(rmag - imag) - 0.1439
    ijmag = rmag - 1.2444*(rmag - imag) - 0.3820

    cr = pyfits.Column('Rmag','1E','mag',array=rjmag)
    ci = pyfits.Column('Imag','1E','mag',array=ijmag)
    cre = pyfits.Column('e_Rmag','1E','cmag',array=e_rmag)
    cie = pyfits.Column('e_Imag','1E','cmag',array=e_imag)

    # removing old ones to prevent duplicity (case insensitive)
    d = pyfits.ColDefs(fits[1].data)
    d.del_col('rmag')
    d.del_col('imag')
    d.del_col('e_rmag')
    d.del_col('e_imag')

    nt = pyfits.new_table(d)

    cols = nt.columns + cr + cre + ci + cie
    f = pyfits.new_table(cols)
    f.writeto(jmuc)

if __name__ == "__main__":
    ucac_jmuc(sys.argv[1],sys.argv[2])

