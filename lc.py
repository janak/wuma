#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Vykreslí světelnou křivku W UMa."""


from numpy import loadtxt
from matplotlib import pyplot as plt


RJD, Rmag, Remag = loadtxt("R.dat", unpack=True)
VJD, Vmag, Vemag = loadtxt("V.dat", unpack=True)
BJD, Bmag, Bemag = loadtxt("B.dat", unpack=True)

plt.title("W UMa - light curve")
plt.xlabel("JD")
plt.ylabel("Mag")
plt.ylim([7, 9.2])
plt.plot(RJD, Rmag, "r.", label="R")
plt.plot(VJD, Vmag, "y.", label="V")
plt.plot(BJD, Bmag, "b.", label="B")
plt.gca().invert_yaxis()
plt.legend()
plt.grid()

plt.savefig("lc.png")


# -------------------------------------------------------------------------- #
# "THE BEER-WARE LICENSE" (Revision 42):                                     #
# <janak@physics.muni.cz> wrote this file. As long as you retain this notice #
# you can do whatever you want with this stuff. If we meet some day, and you #
# think this stuff is worth it, you can buy me a beer in return Zdeněk Janák #
# -------------------------------------------------------------------------- #
