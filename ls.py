#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Výpis snímků podle typu do souboru."""

import os
import glob
from astropy.io import fits


DATA = "20150317"

darky = glob.glob(os.path.join(DATA, "dark*.fits"))

for expozice in [10, 7, 5, 3, 2, 1]:
    with open("dark{}.lst".format(expozice), "w") as dlist:
        for dark in darky:
            if abs(fits.getval(dark, "EXPTIME") - expozice) < 1e-3:
                dlist.write(dark + "\n")


flaty = glob.glob(os.path.join(DATA, "flat_rano*.fits"))

for expozice in [5, 3, 2, 1]:
    for filtr in ["R", "V", "B"]:
        with open("flat{}{}.lst".format(filtr, expozice), "w") as flist:
            for flat in flaty:
                if abs(fits.getval(flat, "EXPTIME") - expozice) < 1e-3 and \
                        fits.getval(flat, "FILTER") == filtr:
                    flist.write(flat + "\n")


# -------------------------------------------------------------------------- #
# "THE BEER-WARE LICENSE" (Revision 42):                                     #
# <janak@physics.muni.cz> wrote this file. As long as you retain this notice #
# you can do whatever you want with this stuff. If we meet some day, and you #
# think this stuff is worth it, you can buy me a beer in return Zdeněk Janák #
# -------------------------------------------------------------------------- #
