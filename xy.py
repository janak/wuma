#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Vykresli polohu W UMa na čipu během pozorování."""


from numpy import loadtxt
from matplotlib import pyplot as plt

x, y = loadtxt("xy.dat", unpack=True)

plt.title("W UMa - x&y @ CCD")
plt.xlabel("x")
plt.ylabel("y")
plt.plot(x, y, ".")
plt.grid(True)

plt.savefig("xy.png")


# -------------------------------------------------------------------------- #
# "THE BEER-WARE LICENSE" (Revision 42):                                     #
# <janak@physics.muni.cz> wrote this file. As long as you retain this notice #
# you can do whatever you want with this stuff. If we meet some day, and you #
# think this stuff is worth it, you can buy me a beer in return Zdeněk Janák #
# -------------------------------------------------------------------------- #
