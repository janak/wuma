#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Pro každý snímek v zadaném adresáři provede jeho binning 2x2
   a nahradí jim původní snímek(!)"""


import os
import sys
import glob
from astropy.io import fits


if len(sys.argv) != 2 or not os.path.isdir(sys.argv[1]):
    sys.exit("Zadej platny nazev adresare.")
else:
    path = sys.argv[1]
    snimky = glob.glob(os.path.join(path, "*.fits"))


for snimek in snimky:

    hdu = fits.open(snimek)
    head = hdu[0].header
    data = hdu[0].data
    hdu.close()

    x, y = data.shape
    xx = x // 2
    yy = y // 2

    binning = data.reshape(xx, 2, yy, 2).sum(axis=3).sum(axis=1)

    head["NAXIS1"] = xx
    head["NAXIS2"] = yy
    head["XBINNING"] = 2
    head["YBINNING"] = 2
    head["HISTORY"] = "Binning 2x2"

    new_hdu = fits.PrimaryHDU(data=binning, header=head)
    new_hdu.writeto(snimek, clobber=True)

    print(".", end="", file=sys.stderr, flush=True)

print("Done!")


# -------------------------------------------------------------------------- #
# "THE BEER-WARE LICENSE" (Revision 42):                                     #
# <janak@physics.muni.cz> wrote this file. As long as you retain this notice #
# you can do whatever you want with this stuff. If we meet some day, and you #
# think this stuff is worth it, you can buy me a beer in return Zdeněk Janák #
# -------------------------------------------------------------------------- #
