#!/bin/bash
#PBS -N WUMa
#PBS -l walltime=24h
#PBS -l nodes=1:ppn=3:x86_64:brno
#PBS -l mem=12gb
#PBS -m abe
#PBS -M janak@physics.muni.cz

## pridani modulu s pythonem
module add python-3.4.1-gcc

## export promennych s cestami k domovskym adresarum
export BRNO=/storage/brno2/home/janak
export KRNO=/storage/brno6/home/janak

## export promennych s cestami k programum
export PATH=$BRNO/.local/bin/:$PATH
export LD_LIBRARY_PATH=$BRNO/.local/lib/:$LD_LIBRARY_PATH

## vypnuti automatickych zaloh
export VERSION_CONTROL=off

## pracovni adresar
WORK=$KRNO/WUMa/
## adresar s daty
DATA=20150317

## prechod do pracovniho adresare
cd $WORK

## roztrideni korekcnich snimku
python ls.py

## vypis surovych snimku do souboru
find $DATA/W_UMa*R*.fits > R.lst
find $DATA/W_UMa*V*.fits > V.lst
find $DATA/W_UMa*B*.fits > B.lst

## jakou verzi munipacku pouzivame?
munipack --version

## vytvorime master-darky pro jednotlive expozice
for i in 10 7 5 3 2 1
do
    munipack dark --verbose -o dark$i.fits @dark$i.lst
done

## vytvorime flaty pro jednotlive filtry a expozice
for f in R V B
do
    for i in 5 3 2 1
    do
        munipack flat --verbose -dark dark$i.fits -o flat$f$i.fits @flat$f$i.lst
    done
done

## vytvorime master-flaty pro jednotlive filtry ze vsech expozic
for f in R V B
do
    munipack flat --verbose -o flat$f.fits flat$f?.fits
done

## fotometricka korekce snimku
munipack --verbose phcorr -t . -flat flatR.fits -dark dark7.fits @R.lst &
munipack --verbose phcorr -t . -flat flatV.fits -dark dark7.fits @V.lst &
munipack --verbose phcorr -t . -flat flatB.fits -dark dark7.fits @B.lst &

wait

## vypis korektovanych snimku do souboru
ls W_UMa*R*.fits > R.lst
ls W_UMa*V*.fits > V.lst
ls W_UMa*B*.fits > B.lst

## hledani hvezd
munipack --verbose find -f 8 @R.lst &
munipack --verbose find -f 8 -th 9 @V.lst &
munipack --verbose find -f 11 -th 13 @B.lst &

wait

## aperturni fotometrie
munipack --verbose aphot @R.lst &
munipack --verbose aphot @V.lst &
munipack --verbose aphot @B.lst &

wait

## dotaz do virtualni observatore na katalog
munipack --verbose cone -r 0.2 145.93945254 55.95252106

## astrometrie
munipack --verbose astrometry -c cone.fits @R.lst &
munipack --verbose astrometry -c cone.fits @V.lst &
munipack --verbose astrometry -c cone.fits @B.lst &

wait

## prepocet hvezdnych velikosti chybejicich filtru v katalogu
python ucac_jmuc.py cone.fits mycone.fits

## absolutni fotometrie
for f in R V B
do
    for s in `cat $f.lst`
    do
        munipack --verbose phcal -c mycone.fits --photsys-ref=Johnson --filters=$f --col-ra=RAJ2000 --col-dec=DEJ2000 --col-mag=${f}mag --col-magerr=e_${f}mag --photsys-instr=MonteBoo --area=0.3 $s
    done
done

## vypis svetelne krivky
for f in R V B
do
    munipack timeseries --stdout -c mag,magerr 145.93945254,55.95252106 W_UMa*$f*_cal.fits > $f.dat
done

## vykresleni svetelne krivky
python lc.py

## ziskani pozice hvezdy na cipu
for v in W_UMa*_????.fits
do
    sky2xy $v 145.93945254 55.95252106 | cut -b 36-55
done > xy.dat

## vykresleni pozice hvezdy na cipu
python xy.py

exit


# -------------------------------------------------------------------------- #
# "THE BEER-WARE LICENSE" (Revision 42):                                     #
# <janak@physics.muni.cz> wrote this file. As long as you retain this notice #
# you can do whatever you want with this stuff. If we meet some day, and you #
# think this stuff is worth it, you can buy me a beer in return Zdeněk Janák #
# -------------------------------------------------------------------------- #
