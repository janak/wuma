# Barevná odlišnost jednovaječných dvojčat

Kdo to byl *Charles Wain* a proč nemohl přijet do Gotwaldova přednést svůj příspěvek na téma barevné odlišnosti jednovaječných dvojčat se už asi nikdy nedozvíme. Ale tohle stejně není příběh o něm. Tohle je příběh o dvou véčkách ve Velké Medvědici.

Video záznam pořízený během jeho návštěvy observatoře na MonteBoo v Krně naleznete [**ZDE**](https://vimeo.com/astrograzl/wuma).
